//
//  Extensions.swift
//  Test
//
//  Created by Midhun P on 21/02/20.
//  Copyright © 2020 Midhun P. All rights reserved.
//

import Foundation
import UIKit



extension String {
    func toDouble() -> Double {
        if let result = NumberFormatter().number(from: self)?.doubleValue {
            return result
        } else {
            return 0
        }
    }
    
    func toFloat() -> Float {
        if let result = NumberFormatter().number(from: self)?.floatValue {
            return result
        } else {
            return 0.0
        }
    }
}


extension UIViewController {
    
    public var activityIndicatorTag: Int { return 999999 }
    public func showActivityIndicator(hideUI: Bool = false, _ style: UIActivityIndicatorView.Style = .whiteLarge, location: CGPoint? = nil) {
        
        if self.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).count < 1 {
            UIApplication.shared.beginIgnoringInteractionEvents()
            let loc = location ?? self.view.center
            
            DispatchQueue.main.async(execute: {
                let activityIndicator = UIActivityIndicatorView(style: style)
                activityIndicator.color = hideUI ? UIColor.clear: UIColor.black
                activityIndicator.tag = self.activityIndicatorTag
                activityIndicator.center = loc
                activityIndicator.hidesWhenStopped = true
                activityIndicator.startAnimating()
                self.view.addSubview(activityIndicator)
            })
        }
    }
    
    
    public func hideActivityIndicator() {
        UIApplication.shared.endIgnoringInteractionEvents()
        DispatchQueue.main.async(execute: {
            if let activityIndicator = self.view.subviews.filter({ $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        })
    }
}

extension NSDictionary {
    
    func getStringValue(keyVal: String) -> String {
        
        let vval = self.value(forKey: keyVal)
        if vval != nil {
            
            let strVal: String = "\(vval!)"
            if strVal == "<null>" {
                return ""
            }
            else {
                return strVal
            }
        }
        else {
            return ""
        }
    }
}
extension UITableView {
    
    public func reloadData(_ completion: @escaping ()->()) {
        UIView.animate(withDuration: 0, animations: {
            self.reloadData()
        }, completion:{ _ in
            completion()
        })
    }
    
    func scroll(to: scrollsTo, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(300)) {
            let numberOfSections = self.numberOfSections
            if numberOfSections > 0 {
                let numberOfRows = self.numberOfRows(inSection: numberOfSections-1)
                switch to{
                case .top:
                    if numberOfRows > 0 {
                        let indexPath = IndexPath(row: 0, section: 0)
                        self.scrollToRow(at: indexPath, at: .top, animated: animated)
                    }
                    break
                case .bottom:
                    if numberOfRows > 0 {
                        let indexPath = IndexPath(row: numberOfRows-1, section: (numberOfSections-1))
                        self.scrollToRow(at: indexPath, at: .bottom, animated: animated)
                    }
                    break
                }
            }
        }
    }
    
    enum scrollsTo {
        case top,bottom
    }
}
