//
//  MeteorDetailViewController.swift
//  Test
//
//  Created by Midhun P on 21/02/20.
//  Copyright © 2020 Midhun P. All rights reserved.
//

import UIKit
import MapKit


class MeteorDetailViewController: UIViewController {

    @IBOutlet weak var mapViewOut: MKMapView!
    
    var singleMeteor:MeteorObj?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        if let dat = singleMeteor {
            
            self.title = dat.descript
            
            let lat = CLLocationDegrees(dat.x)
            let lon = CLLocationDegrees(dat.y)
            let location = CLLocationCoordinate2D(latitude: lon, longitude: lat)
            let span = MKCoordinateSpan(latitudeDelta: 0.08, longitudeDelta: 0.08)
            let region = MKCoordinateRegion(center: location, span: span)
            mapViewOut.setRegion(region, animated: true)
                        
            let annotation = MKPointAnnotation()
            annotation.coordinate = location
            annotation.title = dat.descript
            annotation.subtitle = "\(dat.category), \n\(dat.address)"
            mapViewOut.addAnnotation(annotation)
            
        }
    }

   
}
