//
//  ViewController.swift
//  Test
//
//  Created by Midhun P on 21/02/20.
//  Copyright © 2020 Midhun P. All rights reserved.
//

import UIKit
import RealmSwift
import Toaster
class ViewController: UIViewController {
    
    
    @IBOutlet weak var listTblOut: UITableView!
    @IBOutlet weak var searchBarOut: UISearchBar!
    

    var isSearch: Bool = false
    var dataList: [MeteorObj]?
    
    
    var searchResultDataList: [MeteorObj]?
    let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let searchUIBarButtonItem = UIBarButtonItem(image: UIImage(named: "search"), style: .plain, target: self, action: #selector(ViewController.searchButtonClick))
        self.navigationItem.rightBarButtonItem  = searchUIBarButtonItem
        
        
        self.title = "METEORS LIST"
        listTblOut.delegate = self
        listTblOut.dataSource = self
        listTblOut.register(UINib.init(nibName: "MeteorsTableViewCell", bundle: nil), forCellReuseIdentifier: "MeteorsTableViewCell")
        listTblOut.rowHeight = UITableView.automaticDimension
        listTblOut.estimatedRowHeight = UITableView.automaticDimension
        searchBarOut.delegate = self
        getDataList()
       
        

    }
    override func viewDidAppear(_ animated: Bool) {
      
    }
    
    @objc func searchButtonClick(){
        
        searchBarOut.isHidden = isSearch
        isSearch = !isSearch
        
    }
    
    
    func getDataList() {
        self.getMeteorsListApi(completion: {
            (stat, reason) in
            DispatchQueue.main.async {
                self.hideActivityIndicator()
                if !stat {
                    Toast(text: reason).show()
                }
                self.listTblOut.reloadData()
            }
        })
    }
}


//MARK:- Tableview delgate and datasource
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchResultDataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MeteorsTableViewCell", for: indexPath) as! MeteorsTableViewCell
        
        
        if let dat = searchResultDataList?[indexPath.row] {
            let meteor = MeteorObj()
            
            

        do {
            try realm.write {
                        meteor.descript = dat.descript
                        meteor.address = dat.address
                        meteor.category = dat.category
                        meteor.date = dat.date
                        meteor.incidntnum = dat.incidntnum
                        meteor.x = dat.x
                        meteor.y = dat.y
                        realm.add(meteor)
            }
        }
        catch let error as NSError {
            print("Something went wrong: \(error.localizedDescription)")
        }

            let results = realm.objects(MeteorObj.self)
            
            cell.nameLblout.text = results[indexPath.row].descript
            cell.addressLblout.text = "Address: " + results[indexPath.row].address
            cell.categoryLblout.text = "Category: " + results[indexPath.row].category
            cell.timeLblout.text = "Date: " + results[indexPath.row].date
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dat = self.searchResultDataList?[indexPath.row] {
            if let nextVC = self.storyboard?.instantiateViewController(identifier: "MeteorDetailViewController") as? MeteorDetailViewController {
                nextVC.singleMeteor = dat
                self.navigationController?.pushViewController(nextVC, animated: true)
            }
        }
    }
}


//MARK:- getMeteorsListApi
extension ViewController {
    func getMeteorsListApi(completion: @escaping (_ status: Bool,_ reason: String)-> ()) {
        self.showActivityIndicator()
        ApiHandler.shared.getMeteorsListApi(completion: { (rStatus,rReason,rResponse) in
            if rStatus, let serialisedData = rResponse  {
                self.dataList = serialisedData
                self.searchResultDataList = self.dataList
                completion(true,rReason)
            }
            else {completion(false,rReason)}
        })
    }
}


//MARK:- Searchbar delegate methods
extension ViewController: UIScrollViewDelegate, UISearchBarDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        searchBarOut.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        isSearch = true;
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        isSearch = false;
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText.count == 0 {
            isSearch = false
            self.searchResultDataList = self.dataList
            self.listTblOut.reloadData() {
                self.listTblOut.scroll(to: .top, animated: true)
            }
            
        } else {
            self.searchResultDataList = self.dataList?.filter({ (filtObj) -> Bool in
                
                let filtValue = filtObj.descript
                let text: NSString = filtValue as NSString // as! NSString
                let range = text.range(of: searchText, options: .caseInsensitive)
                return range.location != NSNotFound
            })
            if(self.searchResultDataList?.count == 0) {
                isSearch = false;
            } else {
                isSearch = true;
            }
            self.listTblOut.reloadData() {
                self.listTblOut.scroll(to: .top, animated: true)
            }
        }
    }
}
