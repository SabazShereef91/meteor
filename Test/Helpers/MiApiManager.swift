//
//  MiApiManager.swift
//  youbetterfly
//
//  Created by Midhun P on 28/06/19.
//  Copyright © 2019 Midhun P. All rights reserved.
//

import Foundation
import Alamofire
import SDCAlertView

var zNilResponse = "Something went wrong"
var zNoNetwork = "Please check your internet connection"


let aalamofireManager: SessionManager = {
    let sessionConfiguration = URLSessionConfiguration.default
    sessionConfiguration.timeoutIntervalForRequest = 30
    return Alamofire.SessionManager(configuration: sessionConfiguration)
}()
let aqueue = DispatchQueue(label: "com.cnoon.response-queueq", qos: .utility, attributes: [.concurrent])

class MiApiManager {
    
    static let shared = MiApiManager()
    
    var urlMethod: HTTPMethod!
    func getApiResponse( urlAddress: String, uMethod: HTTPMethod = .post , postData: Parameters, completion: @escaping (_ apiResponse: [NSDictionary]?,_ reason: String,_ status: Bool)-> Void) {
        
        if Reachability()?.isReachable == true {
            let postHeader: HTTPHeaders = ["": ""]
            aalamofireManager.request(urlAddress, method: uMethod, parameters: postData, encoding: URLEncoding.default, headers: postHeader)         
                .responseJSON(queue: aqueue) { response in
                    print("\nparameters-> ",postData," \n\tApiurl-> ",urlAddress," \n\tHeaders-> ", postHeader," \n\tMethod-> ", uMethod," \n")
                    print("respose got in apimanager : ",response,"\n****** \n")
                    switch(response.result) {
                        
                    case .success(_):
                        if response.result.value != nil {
                            
                            if let jSonData = response.result.value as? [NSDictionary] {
                                completion(jSonData,"",true)
                            }
                            else {
                                completion(nil, zNilResponse, false)
                            }
                        }
                        else {
                            completion(nil, zNilResponse, false)
                        }
                    //break
                    case .failure(let error):
                        print(error)
                        completion(nil, zNilResponse, false)
                    }
            }
        }
        else {
            completion(nil, zNoNetwork, false)
            let alert = AlertController(title: "Warning!!", message: "No Internet Connection", preferredStyle: .alert)
     
            alert.addAction(AlertAction(title: "OK", style: .preferred))
            alert.present()

            return
        }
        
    }
    
    
    
    func stopAllSessions() {
        aalamofireManager.session.getAllTasks { tasks in
            tasks.forEach { $0.cancel() }
        }
    }
}
