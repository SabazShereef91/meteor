//
//  ApiHandler.swift
//  engagedxb
//
//  Created by TIMESWORLD MEDIA AND TECHNOLOGY SOLUTIONS on 05/08/19.
//  Copyright © 2019 TIMESWORLD MEDIA AND TECHNOLOGY SOLUTIONS. All rights reserved.
//

import Foundation

class ApiHandler {
    
    static let shared = ApiHandler()
    
    func getMeteorsListApi(params:[String: Any] = [:], completion: @escaping (_ status: Bool,_ reason: String,_ response: [MeteorObj]?)-> Void) {
        print("Api name: getMeteorsListApi")
        let urlAddress = "https://data.sfgov.org/resource/tmnf-yvry.json?$where=:updated_at%20%3E%272011-01-01%27"
        
        MiApiManager.shared.getApiResponse(urlAddress: urlAddress, uMethod: .get,postData: params, completion: {
            (respData,respReason,respStatus) in
            
            if !respStatus {
                completion(false, respReason, nil)
            }
            else {
                if let respDat = respData {
                    let serializedResponse = respDat.map({(val: NSDictionary) -> MeteorObj in
                        let comp = MeteorObj()
                        comp.incidntnum = val.getStringValue(keyVal: "incidntnum")
                        comp.category = val.getStringValue(keyVal: "category")
                        comp.descript = val.getStringValue(keyVal: "descript")
                        comp.date = val.getStringValue(keyVal: "date")
                        comp.address = val.getStringValue(keyVal: "address")
                        comp.x = val.getStringValue(keyVal: "x").toFloat()
                        comp.y = val.getStringValue(keyVal: "y").toFloat()
                        return comp
                    })
                    
                    completion(true,respReason, serializedResponse)
                }
                else {
                    completion(false, respReason, nil)
                }
            }
        })
    }
}
    
