//
//  MeteorsTableViewCell.swift
//  Test
//
//  Created by Midhun P on 21/02/20.
//  Copyright © 2020 Midhun P. All rights reserved.
//

import UIKit

class MeteorsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameLblout: UILabel!
    @IBOutlet weak var addressLblout: UILabel!
    @IBOutlet weak var categoryLblout: UILabel!
    @IBOutlet weak var timeLblout: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
